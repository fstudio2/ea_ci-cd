package edu.miu.cs.cs544.service;

import edu.miu.cs.cs544.domain.Customer;
import edu.miu.cs.cs544.domain.Item;
import edu.miu.cs.cs544.domain.Product;
import edu.miu.cs.cs544.domain.Reservation;
import edu.miu.cs.cs544.dto.ReservationAdapter;
import edu.miu.cs.cs544.dto.ReservationDto;
import edu.miu.cs.cs544.repository.CustomerRepository;
import edu.miu.cs.cs544.repository.ProductRepository;
import edu.miu.cs.cs544.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
/**
 *
 * @Author: Ephrem
 *
 */

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    @Override
    public Reservation getReservationById(Integer id) {
        return reservationRepository.findById(id).orElse(null);
    }


    @Override
    public ReservationDto createReservation(ReservationDto reservationDto) {
        Reservation reservation = ReservationAdapter.getReservation(reservationDto);
        Customer customer = customerRepository.findById(reservationDto.getCustomerId()).orElse(null);
        if (customer == null) {
            throw new IllegalArgumentException("Customer with id " + reservationDto.getCustomerId() + " does not exist");
        }
        reservation.setCustomer(customer);

        return ReservationAdapter.getReservationDto(reservationRepository.save(reservation));
    }

    @Override
    public ReservationDto updateReservation(Integer id, ReservationDto updatedReservationDto) {
        Optional<Reservation> optionalReservation = reservationRepository.findById(id);
        if (optionalReservation.isEmpty()) {
            return null;
        }

        Reservation reservation = ReservationAdapter.getReservation(updatedReservationDto);
        Customer customer = customerRepository.findById(updatedReservationDto.getCustomerId()).orElse(null);
        if (customer == null) {
            throw new IllegalArgumentException("Customer with id " + updatedReservationDto.getCustomerId() + " does not exist");
        }
        reservation.setCustomer(customer);


//        List<Item> items = reservationDto.getItems();
//        for (Item item : items) {
//            Product product= productRepository.findById(item.getProduct().getId()).orElseThrow(null);
//            if (!product.isAvailable()) {
//                throw new IllegalArgumentException("Product with id " + item.getProduct().getId() + " does not exist");
//            }
//        }
        return ReservationAdapter.getReservationDto(reservationRepository.save(reservation));

    }
    @Override
    public void deleteReservation(Integer id) {
        reservationRepository.deleteById(id);
    }
}